# WiFiDirectDemo

#### 介绍
使用wifi direct 实现的文件传输
参考了[Android Developer.Wi-Fi Direct](https://developer.android.com/training/connect-devices-wirelessly/wifi-direct)
和 [leavesC/WifiP2P](https://github.com/leavesC/WifiP2P)
![输入图片说明](https://images.gitee.com/uploads/images/2019/0824/195312_7282a151_1854795.jpeg "Screenshot_20190629-185943_gaitubao_247x439.jpg")
