package com.example.renkangchen.wifidirectdemo.fragment;

import android.annotation.SuppressLint;
import android.app.DownloadManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.renkangchen.wifidirectdemo.R;
import com.example.renkangchen.wifidirectdemo.activity.AboutActivity;
import com.example.renkangchen.wifidirectdemo.activity.SettingActivity;
import com.example.renkangchen.wifidirectdemo.bean.VersionBean;
import com.example.renkangchen.wifidirectdemo.net.VersionApi;
import com.example.renkangchen.wifidirectdemo.util.MyPackageUtils;
import com.example.renkangchen.wifidirectdemo.util.NetUtil;

import java.util.Timer;
import java.util.TimerTask;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static android.content.Context.DOWNLOAD_SERVICE;

public class MeFragment extends Fragment {
    private static final String TAG = "MeFragment";

    private static final int NEW_VERSION_FOUND = 1;

    @BindView(R.id.img_btn_setting)
    ImageButton mImgBtnSetting;
    @BindView(R.id.cardview_setting)
    CardView mCardviewSetting;
    @BindView(R.id.tv_version_info)
    TextView tvVersionInfo;
    @BindView(R.id.cardview_version)
    CardView mCardviewVersion;
    @BindView(R.id.img_btn_about)
    ImageButton mImgBtnAbout;
    @BindView(R.id.cardview_about)
    CardView mCardviewAbout;

    private int mVersionCode;
    private int mNewVersionCode;
    private String mNewVersionMsg;

    private VersionBean.ResultsBean mVersionResultsBean;

    private DownloadManager mDownloadManager;
    private DownloadManager.Request mRequest;

    private TimerTask mTimerTask;

    private int mCurrentLanguageType;

    private long cursorId;

    private String newVersionApkUrl;

    public MeFragment() {
    }

    public static MeFragment newInstance() {
        MeFragment fragment = new MeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);

        return fragment;
    }

    @SuppressLint("HandlerLeak")
    Handler handler =new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            if (msg.what == NEW_VERSION_FOUND) {
                tvVersionInfo.setText(getString(R.string.new_version_found));
                showUpdateDialog();
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getCurrentLanguageType();

        mVersionCode = Integer.valueOf(MyPackageUtils.getPackageVersionName(getActivity())
                .replace(".",""));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_me, container, false);
        ButterKnife.bind(this,view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void getCurrentLanguageType() {
        try {
            SharedPreferences mSharedPreferences = getActivity()
                    .getSharedPreferences("userpreference",
                            Context.MODE_PRIVATE);
            mCurrentLanguageType = mSharedPreferences.getInt("language_type", 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
  }

  private void initDownloadManager() {
        mDownloadManager = (DownloadManager) getActivity().getSystemService(DOWNLOAD_SERVICE);
        mRequest = new DownloadManager.Request(Uri.parse(newVersionApkUrl));

        mRequest.setTitle(getResources().getString(R.string.app_name));
        mRequest.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI|DownloadManager.Request.NETWORK_MOBILE);
        mRequest.setAllowedOverRoaming(false);
        mRequest.setMimeType("application/vnd.android.package-archive");
        mRequest.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

        Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).mkdir();
        mRequest.setDestinationInExternalPublicDir(  Environment.DIRECTORY_DOWNLOADS  , "app-release.apk" );
    }

    private void install(String path) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.parse("file://" + path), "application/vnd.android.package-archive");
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void initDownloadTimerTask() {
        final  DownloadManager.Query query = new DownloadManager.Query();
        Timer mTimer = new Timer();
        mTimerTask = new TimerTask() {
            @Override
            public void run() {
                Cursor cursor = mDownloadManager.query(query.setFilterById(cursorId));
                if (cursor != null && cursor.moveToFirst()) {
                    if (cursor.getInt(
                            cursor.getColumnIndex(DownloadManager.COLUMN_STATUS)) == DownloadManager.STATUS_SUCCESSFUL) {
                        //pb_update.setProgress(100);
                        Message message = handler.obtainMessage();
                        message.what = 1;
                        message.arg1 = 0;

                        handler.sendMessage(message);
                        install(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/app-release.apk" );

                        mTimerTask.cancel();
                    }

                    String title = cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_TITLE));
                    String address = cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));

                    int bytes_downloaded = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR));
                    int bytes_total = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES));
                    int pro =  (bytes_downloaded * 100) / bytes_total;

                    Message msg =Message.obtain();

                    Bundle bundle = new Bundle();
                    bundle.putInt("pro",pro);
                    bundle.putString("name",title);

                    msg.setData(bundle);
                    handler.sendMessage(msg);
                }
                cursor.close();
            }
        };

        mTimer.schedule(mTimerTask, 0,1000);
    }

    private void getNewVersion() {
        Retrofit retrofit = NetUtil.getRetrofitInstance();

        VersionApi versionApi = retrofit.create(VersionApi.class);
        Call<VersionBean> call = versionApi.getLatestVersion();
        call.enqueue(new Callback<VersionBean>() {
            @Override
            public void onResponse(Call<VersionBean> call, Response<VersionBean> response) {
                int code = response.body().getCode();
                if (code == 0) {

                    mVersionResultsBean = response.body().getResults();
                    if (mVersionResultsBean != null) {

                        mNewVersionCode = Integer.valueOf(mVersionResultsBean.getVersion_name().replace(".", ""));
                        newVersionApkUrl = mVersionResultsBean.getVersion_url();

                        initDownloadManager();

                        initDownloadTimerTask();

                        switch (mCurrentLanguageType) {
                            case 1:
                                mNewVersionMsg = mVersionResultsBean.getVersion_info_cn();
                                break;
                            case 2:
                                mNewVersionMsg = mVersionResultsBean.getVersion_info_en();
                                break;
                            case 3:
                                mNewVersionMsg = mVersionResultsBean.getVersion_info_cn_tw();
                                break;
                            default:
                                mNewVersionMsg = mVersionResultsBean.getVersion_info_en();
                                break;
                        }

                        if (mNewVersionCode > mVersionCode) {
                            Message message = handler.obtainMessage();
                            message.what = NEW_VERSION_FOUND;

                            handler.sendMessage(message);
                        }
                    }
                } else {
                    Log.d(TAG, "onResponse: get version update fail ");
                }
            }

            @Override
            public void onFailure(Call<VersionBean> call, Throwable t) {
                Log.d(TAG, "onFailure: ");

            }
        });
    }

    private void showUpdateDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle(getString(R.string.new_version_found))
                .setMessage(mNewVersionMsg)
                .setPositiveButton(getString(R.string.update_immediately), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        cursorId = mDownloadManager.enqueue(mRequest);

                        dialog.dismiss();
                        mTimerTask.run();
                    }
                }).setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.create().show();
    }

    @OnClick({R.id.img_btn_setting, R.id.cardview_setting, R.id.cardview_version, R.id.img_btn_about, R.id.cardview_about})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_btn_setting:
                SettingActivity.activityStart(getActivity());
                break;
            case R.id.cardview_setting:
                SettingActivity.activityStart(getActivity());
                break;
            case R.id.cardview_version:
                //getNewVersion();
                break;
            case R.id.img_btn_about:
                AboutActivity.activityStart(getActivity());
                break;
            case R.id.cardview_about:
                AboutActivity.activityStart(getActivity());
                break;
        }
    }
}
