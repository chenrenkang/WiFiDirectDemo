package com.example.renkangchen.wifidirectdemo.util;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

public class MyPackageUtils {


    /**
     *
     * @param context
     * @return
     */
    public static String getPackageVersionName(Context context) {
        PackageManager manager = context.getPackageManager();
        StringBuffer stringBuffer = new StringBuffer();
        try {
            PackageInfo info = manager.getPackageInfo(context.getPackageName(), 0);
            stringBuffer.append(info.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return stringBuffer.toString();
    }

}
