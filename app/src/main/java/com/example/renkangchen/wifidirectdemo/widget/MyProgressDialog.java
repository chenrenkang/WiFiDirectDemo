package com.example.renkangchen.wifidirectdemo.widget;

import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.renkangchen.wifidirectdemo.R;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MyProgressDialog extends Dialog {
    private static final String TAG = "MyProgressDialog";
    @BindView(R.id.tv_progress_title)
    TextView tvProgressTitle;
    @BindView(R.id.tv_progress_percentage)
    TextView tvProgressPercentage;
    @BindView(R.id.progressbar)
    ProgressBar progressbar;

    public MyProgressDialog(@NonNull Context context) {
        super(context);

        setContentView(R.layout.my_progress_dialog_layout);
        ButterKnife.bind(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ViewGroup.LayoutParams params = getWindow().getAttributes();
        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT;

        getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);
    }

    public void show(String msg,boolean cancelable,boolean canceledOnTouchOutside) {
        setCancelable(cancelable);
        setCanceledOnTouchOutside(canceledOnTouchOutside);

        if (tvProgressTitle != null) {
            tvProgressTitle.setText(msg);
        }

        show();
    }

    public void updateProgress(int progress) {
        if (tvProgressPercentage != null) {
            String msg = String.format(
                    getContext().getResources()
                            .getString(R.string.transfer_file_now),
                    String.valueOf(progress));

            tvProgressPercentage.setText(msg);
        }

        if (progressbar != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                progressbar.setProgress(progress,true);
            }
            progressbar.setProgress(progress);
        }
    }

}
