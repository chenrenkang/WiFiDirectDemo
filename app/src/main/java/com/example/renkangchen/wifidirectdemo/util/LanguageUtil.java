package com.example.renkangchen.wifidirectdemo.util;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;

import java.util.Locale;

public class LanguageUtil {
    private static final String TAG = "LanguageUtil";
    /**
     *
     * @param context
     * @param type
     */
    public static void changeLanguage(Context context,int type) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        Configuration configuration = context.getResources().getConfiguration();
        Locale locale = null;
        switch (type) {
            case 0:
                locale = Locale.getDefault();
                break;
            case 1:
                locale = Locale.SIMPLIFIED_CHINESE;
                break;
            case 2:
                locale = Locale.ENGLISH;
                break;
            case 3:
                locale = Locale.TRADITIONAL_CHINESE;
                break;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            configuration.setLocale(locale);
        } else {
            configuration.locale = locale;
        }
        context.getResources().updateConfiguration(configuration, displayMetrics);
    }
}
