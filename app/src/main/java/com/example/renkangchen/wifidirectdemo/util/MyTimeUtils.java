package com.example.renkangchen.wifidirectdemo.util;

import android.content.Context;
import android.util.Log;

import com.example.renkangchen.wifidirectdemo.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MyTimeUtils {
    private static final String TAG = "MyTimeUtils";
    /**
     * convert sec into 00:00:00,for example 10 -> 00:00:10
     *
     * @param sec
     * @return
     */
    public static String secondeFormate(int sec) {
        StringBuffer stringBuffer = new StringBuffer();
        int temp = 0;
        temp = sec / 3600;
        stringBuffer.append((temp<10) ? "0"+temp+":" : ""+temp+":");
        temp = sec % 3600 /60;
        stringBuffer.append((temp<10) ? "0"+temp+":" : ""+temp+":");
        temp = sec % 3600 % 60;
        stringBuffer.append((temp<10) ? "0"+temp : ""+temp);

        return stringBuffer.toString();
    }

    /**
     * get string of date time distance
     * just now,minute before...
     *
     * @param dateTime
     * @return
     */
    public static  String  dataTimeToNow(Context context,String dateTime) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        /** millis **/
        long time = 0L;
        long timeNow = 0L;
        Date date = new Date();
        try {
            time = simpleDateFormat.parse(dateTime).getTime()/1000;
            timeNow = date.getTime()/1000;
            long distance = timeNow - time;
            if (distance < 60) {
                return context.getString(R.string.just_now);
            } else if(distance < 60*5) {
                return context.getString(R.string.few_minutes_ago);
            } else if (distance < 60*60*24) {
                return dateTime.substring(11);
            } else {
                return dateTime;
            }

        } catch (ParseException e) {
            Log.d(TAG, "dataTimeToNow: "+e.getMessage());
            return dateTime;
        }
    }

    /**
     *
     * @param millsecond
     * @return
     */
    public static String millsecond2Date(long millsecond) {
        Date date = new Date(millsecond);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
        return simpleDateFormat.format(date);
    }
}
