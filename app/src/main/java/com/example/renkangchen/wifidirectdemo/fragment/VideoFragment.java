package com.example.renkangchen.wifidirectdemo.fragment;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.renkangchen.wifidirectdemo.bean.VideoItem;
import com.example.renkangchen.wifidirectdemo.R;
import com.example.renkangchen.wifidirectdemo.adapter.VideoListAdapter;
import com.example.renkangchen.wifidirectdemo.util.FileUtils;
import com.example.renkangchen.wifidirectdemo.util.MyTimeUtils;

import java.util.ArrayList;
import java.util.List;

public class VideoFragment extends Fragment {
    private static final String TAG = "VideoFragment";

    private List<VideoItem> mVideoItemList = new ArrayList<>();
    private RecyclerView mRecyclerView;

    private VideoListAdapter mVideoListAdapter;

    private OnVideoFragmentInteractionListener mListener;

    public VideoFragment() {

    }

    public static VideoFragment newInstance() {
        VideoFragment fragment = new VideoFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_video, container, false);
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            mRecyclerView = (RecyclerView) view;
            mRecyclerView.setLayoutManager(new LinearLayoutManager(context));

            updateVideoList();

            mVideoListAdapter = new VideoListAdapter(mVideoItemList,mListener);
            /*
             * avoid the blinking problem after remove item
             * should setItemAnimator(null),or use setHasStableIds(true)
             * */
            mVideoListAdapter.setHasStableIds(true);
            mRecyclerView.setAdapter(mVideoListAdapter);

            mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        }
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnVideoFragmentInteractionListener) {
            mListener = (OnVideoFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnVideoFragmentInteractionListener {
        // TODO: Update argument type and name
        void onVideoFragmentItemClick(VideoItem videoItem);
        void onVideoFragmentItemMore(VideoItem videoItem, int position);
    }


    private void updateVideoList() {
        mVideoItemList.clear();
        VideoListUpdateTask mVideoListUpdateTask = new VideoListUpdateTask();
        mVideoListUpdateTask.execute();
    }


    public void removeData(int position) {
       mVideoListAdapter.removeData(position);
    }

    private class VideoListUpdateTask extends AsyncTask<Object,VideoItem,Void> {
        public VideoListUpdateTask() {
            super();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }

        @Override
        protected void onProgressUpdate(VideoItem... values) {
            super.onProgressUpdate(values);

            VideoItem videoItem = values[0];
            mVideoItemList.add(videoItem);
            mRecyclerView.getAdapter().notifyDataSetChanged();
        }

        @Override
        protected void onCancelled(Void aVoid) {
            super.onCancelled(aVoid);
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        @Override
        protected Void doInBackground(Object... objects) {
            Uri uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
            String [] searchKey = new String[] {

                    MediaStore.Video.Media.TITLE,
                    MediaStore.Video.Media.DURATION,
                    MediaStore.Video.Media.SIZE,
                    MediaStore.Video.Media.DATE_ADDED,
                    MediaStore.Images.Media.DATA
            };

            String where = null;//scan all video in media store
            String [] keywords = null;
            String sortOrder = MediaStore.Video.Media.DEFAULT_SORT_ORDER;

            ContentResolver contentResolver = getActivity().getContentResolver();
            Cursor cursor = contentResolver.query(uri,searchKey,where,keywords,sortOrder);

            if (cursor != null) {
                while (cursor.moveToNext() && !isCancelled()) {
                    String videoPath = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA));

                    MediaMetadataRetriever retr = new MediaMetadataRetriever();
                    try {
                        retr.setDataSource(getContext(),Uri.parse(videoPath));
                    } catch (Exception e) {
                        e.printStackTrace();
                        continue;
                    }
                    String height = retr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT);
                    String width = retr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH);

                    String videoResolution = width+"x"+height;

                    String title = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.TITLE));
                    Long duration = cursor.getLong(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DURATION));
                    String date_add = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATE_ADDED));
                    Long videoSize = cursor.getLong(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.SIZE));

                    int duration2Second = (int) (duration/1000);

                    VideoItem videoItem = new VideoItem(title,MyTimeUtils.secondeFormate(duration2Second),
                            videoResolution,String.valueOf(videoSize/1024/1024),
                            MyTimeUtils.millsecond2Date(Long.valueOf(date_add)*1000L),
                            videoPath,
                            "");

                    publishProgress(videoItem);

                }
                cursor.close();
            } else {
                //TODO no video found
            }
            return null;
        }
    }
}
