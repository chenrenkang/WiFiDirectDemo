package com.example.renkangchen.wifidirectdemo.service;

import android.app.IntentService;
import android.content.Intent;
import android.os.Binder;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;

import com.example.renkangchen.wifidirectdemo.bean.FileTransferBean;
import com.example.renkangchen.wifidirectdemo.listener.FileTransferProgressListener;
import com.example.renkangchen.wifidirectdemo.util.FileUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;

import androidx.annotation.Nullable;


/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions and extra parameters.
 */
public class WiFiP2PServerService extends IntentService {
    private static final String TAG = "WiFiP2PServerService";

    private static FileTransferProgressListener fileTransferProgressListener;

    private ServerSocket serverSocket;
    final int serverPort = 8888;

    private InputStream inputStream;
    private ObjectInputStream objectInputStream;
    private FileOutputStream fileOutputStream;

    private int priorProgress = 0;

    public WiFiP2PServerService() {
        super("WiFiP2PServerService");
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return new MyBinder();
    }

    public class MyBinder extends Binder {
        public WiFiP2PServerService getService() {
            return WiFiP2PServerService.this;
        }
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        cleanUp();
        File file = null;
        String client_ip = "";
        try {
            serverSocket = new ServerSocket(serverPort);
            Log.d(TAG, "onHandleIntent: Server:Socket Opened on port "+serverPort);

            Socket client = serverSocket.accept();
            Log.d(TAG, "onHandleIntent: Server:Socket Connection Done");
            Log.d(TAG, "onHandleIntent: Client ip address: "+client.getInetAddress().getHostAddress());
            client_ip = client.getInetAddress().getHostAddress();

            inputStream = client.getInputStream();
            objectInputStream = new ObjectInputStream(inputStream);
            FileTransferBean fileTransferBean = (FileTransferBean)
                    objectInputStream.readObject();


            Log.d(TAG, "onHandleIntent: file to accept is: "+fileTransferBean);

            file = new File(Environment.getExternalStorageDirectory()
                    +"/"+getPackageName()
                    +"/"+fileTransferBean.getFileName());

            boolean result;
            if (!file.getParentFile().exists()) {
                result = file.getParentFile().mkdirs();
                Log.d(TAG, "onHandleIntent: parent dir make result: "+result);
            }
            if (!file.exists()) {
                result = file.createNewFile();
                Log.d(TAG, "onHandleIntent: new file create result: "+result);
            }

            fileOutputStream = new FileOutputStream(file);

            byte[] buf = new byte[512];
            int len;
            long total = 0;
            int progress;
            while((len = inputStream.read(buf)) != -1) {
                fileOutputStream.write(buf,0,len);
                total += len;
                progress = (int)((total * 100) / fileTransferBean.getFileLength());

                Log.d(TAG, "onHandleIntent: File Receive Progress ---->"+progress);

                if (fileTransferProgressListener != null) {
                    if ((progress % 5 == 0) && (priorProgress != progress)) {
                        priorProgress = progress;
                        fileTransferProgressListener
                                .onFileTransferProgressChanged(fileTransferBean, progress);
                    }
                } else {
                    Log.d(TAG, "onHandleIntent: fileTransferProgressListener is null");
                }
            }

            serverSocket.close();
            inputStream.close();
            objectInputStream.close();
            fileOutputStream.close();

            serverSocket = null;
            inputStream = null;
            objectInputStream = null;
            fileOutputStream = null;

            Log.d(TAG, "onHandleIntent: File Received:MD5 is"
                        +FileUtils.getFileMD5(file));

            if (fileTransferProgressListener != null) {
                String originalFileMD5 = fileTransferBean.getFileMD5();
                String fileMD5  = FileUtils.getFileMD5(file);

                fileTransferProgressListener
                        .onFileVerifyResult(originalFileMD5.equals(fileMD5));
            }

            } catch (Exception e) {
                Log.d(TAG, "onHandleIntent: File Receive Exception:"+e.getMessage());
            } finally {
                cleanUp();
                if ((fileTransferProgressListener != null) && file != null) {
                    fileTransferProgressListener.onFilTransferDone(file,client_ip);
                }
                startService(new Intent(this,WiFiP2PServerService.class));
            }
    }


    public static  void setFileTransferProgressListener(FileTransferProgressListener listener) {
        fileTransferProgressListener = listener;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        cleanUp();
    }

    private void cleanUp() {
        if (serverSocket != null) {
            try {
                serverSocket.close();
                serverSocket = null;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (inputStream != null) {
            try {
                inputStream.close();
                inputStream = null;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (objectInputStream != null) {
            try {
                objectInputStream.close();
                objectInputStream = null;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (fileOutputStream != null) {
            try {
                fileOutputStream.close();
                fileOutputStream = null;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
