package com.example.renkangchen.wifidirectdemo.listener;

import com.example.renkangchen.wifidirectdemo.bean.FileTransferBean;

import java.io.File;

public interface FileTransferProgressListener {
    public void onFileTransferProgressChanged(FileTransferBean fileTransferBean, int progress);

    public void onFilTransferDone(File file, String client_ip);

    public void onFileVerifyResult(boolean fileVerifyResult);
}
