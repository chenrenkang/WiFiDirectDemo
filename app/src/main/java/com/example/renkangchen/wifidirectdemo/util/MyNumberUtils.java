package com.example.renkangchen.wifidirectdemo.util;

public class MyNumberUtils {

    /**
     * convert a number into K,W
     *
     * @param number
     * @return
     */
    public static String convertNumber2String(int number) {
        StringBuffer stringBuffer = new StringBuffer();
        int tmp;

        if (number > 10000) {
            tmp = number / 10000;
            stringBuffer.append(tmp+"w");
        } else if (number > 1000) {
            tmp = number / 1000;
            stringBuffer.append(tmp+"k");
        } else {
            stringBuffer.append(number);
        }

        return stringBuffer.toString();
    }
}
