package com.example.renkangchen.wifidirectdemo.util;

public class CMDUtils {
    /**
     * split a string cmd to string array
     * @param cmd
     * @return
     */
    public static String[] splitCmd(String cmd) {
        String regulation = "[ \\t]+";
        final String[] split = cmd.split(regulation);
        return  split;
    }

}
