package com.example.renkangchen.wifidirectdemo.activity;

import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.renkangchen.wifidirectdemo.bean.VideoItem;
import com.example.renkangchen.wifidirectdemo.R;
import com.example.renkangchen.wifidirectdemo.fragment.MeFragment;
import com.example.renkangchen.wifidirectdemo.fragment.VideoFragment;
import com.example.renkangchen.wifidirectdemo.util.MyTimeUtils;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, BottomNavigationView.OnNavigationItemSelectedListener,
        VideoFragment.OnVideoFragmentInteractionListener {
    private static final String TAG = "MainActivity";

    private static final int SELECT_ITEM = 0;
    private static final int DETAIL_ITEM = 1;
    private static final int REMOVE_ITEM = 2;

    private static final int FILE_CHOOSE_REQUEST_CODE = 1;

    /** for record twice back press **/
    private long time = 0;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.bottom_navigation)
    BottomNavigationView mBottomNavigation;
    @BindView(R.id.nav_view)
    NavigationView mNavView;
    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;

    private VideoFragment mVideoFragment;
    private MeFragment mMeFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        initData();
        initView();
    }

    private void initData() {

    }

    private void initView() {
        setSupportActionBar(mToolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        mNavView.setNavigationItemSelectedListener(this);

        mBottomNavigation.setOnNavigationItemSelectedListener(this);

        setDefaultFragment();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onVideoFragmentItemClick(VideoItem videoItem) {
        DeviceActivity.activityStart(this,videoItem);
    }

    @Override
    public void onVideoFragmentItemMore(VideoItem videoItem, int position) {
        showVideoItemMoreDialog(videoItem,position);
    }

    public void showVideoItemMoreDialog(VideoItem videoItem, int position) {
        String[] items = getResources().getStringArray(R.array.video_item_more);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case SELECT_ITEM:
                        DeviceActivity.activityStart(MainActivity.this,videoItem);
                        break;
                    case DETAIL_ITEM:
                        VideoDetailActivity.activityStart(MainActivity.this,videoItem);
                        break;
                    case REMOVE_ITEM:
                        mVideoFragment.removeData(position);
                        break;
                }
            }
        });

        builder.create().show();
    }

    /**
     * set the default fragment for main activity
     */
    private void setDefaultFragment() {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        mVideoFragment = VideoFragment.newInstance();
        transaction.replace(R.id.fragment_content,mVideoFragment);
        transaction.commit();
    }

    private void hideFragment(FragmentTransaction transaction) {
        if(mVideoFragment != null) {
            transaction.hide(mVideoFragment);
        }
        if (mMeFragment != null) {
            transaction.hide(mMeFragment);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        boolean isBottomNav = false;
        FragmentManager fm = this.getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        hideFragment(transaction);

        switch (id) {
            case R.id.nav_open_file:
                chooseFile();
                break;
            case R.id.nav_setting:
                SettingActivity.activityStart(MainActivity.this);
                break;
            case R.id.nav_exit:
                finish();
                break;
            case R.id.bottom_navigation_video:
                isBottomNav = true;

                if (mVideoFragment == null) {
                    mVideoFragment = VideoFragment.newInstance();
                    transaction.add(R.id.fragment_content,mVideoFragment);
                } else {
                    transaction.show(mVideoFragment);
                }
                break;
            case R.id.bottom_navigation_me:
                isBottomNav = true;

                if (mMeFragment == null) {
                    mMeFragment = MeFragment.newInstance();
                    transaction.add(R.id.fragment_content,mMeFragment);
                } else {
                    transaction.show(mMeFragment);
                }
                break;
        }

        if (isBottomNav) {
            transaction.commit();
        } else {
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
        }

        return true;
    }

    private void chooseFile() {
        Intent intent = new Intent();
        intent.setType("image/*;video/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent,FILE_CHOOSE_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FILE_CHOOSE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Uri uri = data.getData();

                String [] searchKey = new String[] {
                        MediaStore.Video.Media.TITLE,
                        MediaStore.Video.Media.DURATION,
                        MediaStore.Video.Media.SIZE,
                        MediaStore.Video.Media.DATE_ADDED,
                        MediaStore.Images.Media.DATA
                };

                ContentResolver contentResolver = getContentResolver();
                Cursor cursor = contentResolver.query(uri,searchKey,null,null,null);

                cursor.moveToFirst();

                String videoPath = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA));

                MediaMetadataRetriever retr = new MediaMetadataRetriever();
                try {
                    retr.setDataSource(this, Uri.parse(videoPath));
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d(TAG, "onActivityResult: error" + e.getMessage());
                }

                String height = retr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT);
                String width = retr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH);
                String videoResolution = width + "x" + height;

                String title = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.TITLE));
                Long duration = cursor.getLong(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DURATION));
                String date_add = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATE_ADDED));
                Long videoSize = cursor.getLong(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.SIZE));

                int duration2Second = (int) (duration / 1000);

                VideoItem videoItem = new VideoItem(title, MyTimeUtils.secondeFormate(duration2Second),
                        videoResolution, String.valueOf(videoSize / 1024 / 1024),
                        MyTimeUtils.millsecond2Date(Long.valueOf(date_add) * 1000L),
                        videoPath,
                        "");

                cursor.close();
                DeviceActivity.activityStart(this,videoItem);
            }
        }
    }

    public boolean onKeyDown(int KeyCode, KeyEvent event) {
        if (KeyCode == KeyEvent.KEYCODE_BACK) {
            exit();
            return false;
        }
        return super.onKeyDown(KeyCode, event);
    }

    public void exit() {
        if ((System.currentTimeMillis() - time) > 2000) {
            Toast.makeText(this,
                    getResources().getString(R.string.press_again_exit), Toast.LENGTH_SHORT).show();
            time = System.currentTimeMillis();
        } else {
            finish();
        }
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }
}