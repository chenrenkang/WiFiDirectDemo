package com.example.renkangchen.wifidirectdemo.bean;

import java.io.File;
import java.io.Serializable;

import androidx.annotation.NonNull;

public class FileTransferBean implements Serializable {
    private String filePath;
    private String fileName;
    private long fileLength;
    private String fileMD5;

    public FileTransferBean() {
    }

    public FileTransferBean(String filePath, long fileLength) {
        this.filePath = filePath;
        this.fileLength = fileLength;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFileName() {
        fileName = (new File(filePath)).getName();
        return fileName;
    }

    public long getFileLength() {
        return fileLength;
    }

    public void setFileLength(long fileLength) {
        this.fileLength = fileLength;
    }

    public String getFileMD5() {
        return fileMD5;
    }

    public void setFileMD5(String fileMD5) {
        this.fileMD5 = fileMD5;
    }

    @NonNull
    @Override
    public String toString() {
        return "FileTransferBean {"+
                "filePath = "+filePath+";"+
                "fileLength = "+fileLength+";"+
                "fileMD5 = "+fileMD5+"}";
    }
}
