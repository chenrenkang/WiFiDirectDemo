package com.example.renkangchen.wifidirectdemo.bean;

import java.util.List;

public class VersionBean {
    private int code;
    private String message;
    private ResultsBean results;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResultsBean getResults() {
        return results;
    }

    public void setResults(ResultsBean results) {
        this.results = results;
    }

    public static class ResultsBean {

        /**
         * id : 1
         * version_name : 1.1
         * version_type : beta
         * version_url : http://47.107.191.64/tmp/app-debug.apk
         * version_info_cn : 版本更新测试
         * version_info_en : Version update test
         * version_info_cn_tw : 版本更新測試
         */

        private int id;
        private String version_name;
        private String version_type;
        private String version_url;
        private String version_info_cn;
        private String version_info_en;
        private String version_info_cn_tw;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getVersion_name() {
            return version_name;
        }

        public void setVersion_name(String version_name) {
            this.version_name = version_name;
        }

        public String getVersion_type() {
            return version_type;
        }

        public void setVersion_type(String version_type) {
            this.version_type = version_type;
        }

        public String getVersion_url() {
            return version_url;
        }

        public void setVersion_url(String version_url) {
            this.version_url = version_url;
        }

        public String getVersion_info_cn() {
            return version_info_cn;
        }

        public void setVersion_info_cn(String version_info_cn) {
            this.version_info_cn = version_info_cn;
        }

        public String getVersion_info_en() {
            return version_info_en;
        }

        public void setVersion_info_en(String version_info_en) {
            this.version_info_en = version_info_en;
        }

        public String getVersion_info_cn_tw() {
            return version_info_cn_tw;
        }

        public void setVersion_info_cn_tw(String version_info_cn_tw) {
            this.version_info_cn_tw = version_info_cn_tw;
        }
    }
}
