package com.example.renkangchen.wifidirectdemo.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.example.renkangchen.wifidirectdemo.R;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;

public class AboutActivity extends AppCompatActivity {

    @BindView(R.id.custom_toolbar)
    Toolbar mCustomToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        ButterKnife.bind(this);

        initData();
        initView();
    }

    private void initData() {

    }

    private void initView() {
        mCustomToolbar.setTitle(getResources().getString(R.string.about));
        mCustomToolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));

        setSupportActionBar(mCustomToolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public static void activityStart(Context context) {
        Intent intent = new Intent(context, AboutActivity.class);
        context.startActivity(intent);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }
}
