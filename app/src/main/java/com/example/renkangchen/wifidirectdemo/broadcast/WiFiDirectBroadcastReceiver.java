package com.example.renkangchen.wifidirectdemo.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.util.Log;

import com.example.renkangchen.wifidirectdemo.activity.DeviceActivity;
import com.example.renkangchen.wifidirectdemo.listener.WiFiDirectActionListener;

public class WiFiDirectBroadcastReceiver extends BroadcastReceiver {
    private static final String TAG = "WiFiDirectBroadcastRece";

    private WifiP2pManager mWifiP2pManager;
    private WifiP2pManager.Channel mChannel;
    private WiFiDirectActionListener mWiFiDirectActionListener;

    public WiFiDirectBroadcastReceiver(WifiP2pManager mWifiP2pManager, WifiP2pManager.Channel mChannel, WiFiDirectActionListener mWiFiDirectActionListener) {
        this.mWifiP2pManager = mWifiP2pManager;
        this.mChannel = mChannel;
        this.mWiFiDirectActionListener = mWiFiDirectActionListener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        switch (action) {
            case WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION:
                int extraWifiState = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE,-1);

                if(extraWifiState == WifiP2pManager.WIFI_P2P_STATE_ENABLED) {
                    mWiFiDirectActionListener.onWiFiP2PEnabled(true);
                } else {
                    mWiFiDirectActionListener.onWiFiP2PEnabled(false);
                }
                break;

            case WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION:
                mWifiP2pManager.requestPeers(mChannel, new WifiP2pManager.PeerListListener() {
                    @Override
                    public void onPeersAvailable(WifiP2pDeviceList peers) {
                        mWiFiDirectActionListener.onPeersAvailable(peers.getDeviceList(),peers);
                    }
                });
                break;

            case WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION:
                NetworkInfo networkInfo = (NetworkInfo) intent
                        .getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO);
                if (networkInfo.isConnected()) {
                    mWifiP2pManager.requestConnectionInfo(mChannel, new WifiP2pManager.ConnectionInfoListener() {
                        @Override
                        public void onConnectionInfoAvailable(WifiP2pInfo info) {
                            mWiFiDirectActionListener.onConnectionInfoAvailable(info);
                        }
                    });
                    Log.d(TAG, "onReceive: p2p device connected");
                } else {
                    mWiFiDirectActionListener.onDisconnection();
                    Log.d(TAG, "onReceive: p2p device disconnected");
                }
                break;

            case WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION:
                WifiP2pDevice device = (WifiP2pDevice) intent
                        .getParcelableExtra(WifiP2pManager.EXTRA_WIFI_P2P_DEVICE);
                mWiFiDirectActionListener.onSelfDeviceAvailable(device);
                break;

            case WifiP2pManager.WIFI_P2P_DISCOVERY_CHANGED_ACTION:
                int extraDiscoveryState = intent.getIntExtra(WifiP2pManager.EXTRA_DISCOVERY_STATE,-1);
                mWiFiDirectActionListener.onWiFiDirectDiscoveryState(extraDiscoveryState);
                break;

            default:
                throw new UnsupportedOperationException("Not yet implemented");
        }

    }
}
