package com.example.renkangchen.wifidirectdemo.listener;

import com.example.renkangchen.wifidirectdemo.bean.DeviceItem;

public interface OnDeviceListItemListener {
    /**
     *
     * @param deviceItem
     */
    void onDeviceItemClick(DeviceItem deviceItem);

    /**
     *
     * @param deviceItem
     */
    void onDeviceItemMoreClick(DeviceItem deviceItem);
}
