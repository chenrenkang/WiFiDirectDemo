package com.example.renkangchen.wifidirectdemo.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.wifi.p2p.WifiP2pDevice;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.renkangchen.wifidirectdemo.R;
import com.example.renkangchen.wifidirectdemo.bean.DeviceItem;
import com.example.renkangchen.wifidirectdemo.listener.OnDeviceListItemListener;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class DeviceListAdapter extends RecyclerView.Adapter<DeviceListAdapter.ViewHolder> {
    private Context mContext;
    private final List<DeviceItem> mValues;
    private OnDeviceListItemListener mListener;

    public DeviceListAdapter(Context mContext, List<DeviceItem> mValues, OnDeviceListItemListener mListener) {
        this.mContext = mContext;
        this.mValues = mValues;
        this.mListener = mListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.device_item_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);

        holder.tvDeviceName.setText(holder.mItem.getDeviceName());
        holder.tvDeviceStatus.setText(holder.mItem.getDeviceStatus());

        switch (holder.mItem.getWifiP2pDevice().status) {
            case WifiP2pDevice.CONNECTED:
                holder.viewDeviceStatus.setBackgroundColor(
                        mContext.getResources().getColor(R.color.colorGreen));
                break;
            default:
                holder.viewDeviceStatus.setBackgroundColor(
                        mContext.getResources().getColor(R.color.colorGray));
                break;
        }

        holder.cardViewDevice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onDeviceItemClick(holder.mItem);
            }
        });

        holder.imgBtnDeviceMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onDeviceItemMoreClick(holder.mItem);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public DeviceItem mItem;

        @BindView(R.id.view_device_status)
        View viewDeviceStatus;
        @BindView(R.id.tv_device_name)
        TextView tvDeviceName;
        @BindView(R.id.tv_device_status)
        TextView tvDeviceStatus;
        @BindView(R.id.img_btn_device_more)
        ImageButton imgBtnDeviceMore;
        @BindView(R.id.cardview_device)
        CardView cardViewDevice;

        public ViewHolder(View mView) {
            super(mView);
            this.mView = mView;
            ButterKnife.bind(this,mView);
        }
    }
}
