package com.example.renkangchen.wifidirectdemo.util;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
public class NetUtil {


    public static Retrofit getRetrofitInstance() {
        return new Retrofit.Builder()
                .baseUrl("http://your_server_addr/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
}
