package com.example.renkangchen.wifidirectdemo.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.example.renkangchen.wifidirectdemo.R;
import com.example.renkangchen.wifidirectdemo.bean.VideoItem;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;

public class VideoPlayActivity extends AppCompatActivity {
    private static final String TAG = "VideoPlayActivity";
    @BindView(R.id.custom_toolbar)
    Toolbar mCustomToolbar;
    @BindView(R.id.play_activity_playerview)
    PlayerView mPlayActivityPlayerview;

    private VideoItem mVideoItem;

    private SimpleExoPlayer mSimpleExoPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_play);
        ButterKnife.bind(this);

        initData();
        initView();
    }

    void initData() {
        mVideoItem = (VideoItem) getIntent().getSerializableExtra("video_item");
    }

    void initView() {
        mCustomToolbar.setTitle(mVideoItem.getVideoName());
        mCustomToolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));

        setSupportActionBar(mCustomToolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initExoPlayer() {
        if (mSimpleExoPlayer == null) {
            mSimpleExoPlayer = ExoPlayerFactory
                    .newSimpleInstance(new DefaultRenderersFactory(this),
                            new DefaultTrackSelector(), new DefaultLoadControl());
        }

        // Bind the player to the view.
        mPlayActivityPlayerview.setPlayer(mSimpleExoPlayer);
        mPlayActivityPlayerview.hideController();
        // Produces DataSource instances through which media data is loaded.
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(this,
                Util.getUserAgent(this, "yourApplicationName"));
        // This is the MediaSource representing the media to be played.
        MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory)
                .createMediaSource(Uri.parse(mVideoItem.getVideoPath()));
        // Prepare the player with the source.
        mSimpleExoPlayer.prepare(videoSource);

        mSimpleExoPlayer.setPlayWhenReady(true);
    }

    private void releasePlayer() {
        if (mSimpleExoPlayer != null) {
            mSimpleExoPlayer.release();
            mSimpleExoPlayer = null;
        }
    }

    /**
     *
     * @param context
     * @param videoItem
     */
    public static void activityStart(Context context, VideoItem videoItem) {
        Intent intent = new Intent(context, VideoPlayActivity.class);
        intent.putExtra("video_item", videoItem);
        context.startActivity(intent);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initExoPlayer();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        releasePlayer();
    }

    @Override
    protected void onStop() {
        super.onStop();
        releasePlayer();
    }
}
