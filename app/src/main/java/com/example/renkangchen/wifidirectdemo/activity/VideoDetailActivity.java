package com.example.renkangchen.wifidirectdemo.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.renkangchen.wifidirectdemo.R;
import com.example.renkangchen.wifidirectdemo.bean.VideoItem;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VideoDetailActivity extends AppCompatActivity {

    @BindView(R.id.iv_detail)
    ImageView ivDetail;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.collapsing_toolbar_layout)
    CollapsingToolbarLayout collapsingToolbarLayout;
    @BindView(R.id.appBar_layout)
    AppBarLayout appBarLayout;
    @BindView(R.id.tv_detail_path)
    TextView tvDetailPath;
    @BindView(R.id.tv_detail_resolution)
    TextView tvDetailResolution;
    @BindView(R.id.tv_detail_size)
    TextView tvDetailSize;
    @BindView(R.id.tv_detail_duration)
    TextView tvDetailDuration;
    @BindView(R.id.tv_detail_date)
    TextView tvDetailDate;
    @BindView(R.id.float_btn_play)
    FloatingActionButton floatBtnPlay;
    @BindView(R.id.coordinator_toolbar)
    CoordinatorLayout coordinatorToolbar;

    private VideoItem mVideoItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_detail);
        ButterKnife.bind(this);

        initData();
        initView();
    }

    private void initData() {
        mVideoItem = (VideoItem) getIntent().getSerializableExtra("video_item");

    }

    private void initView() {
        toolbar.setTitle(mVideoItem.getVideoName());
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Glide.with(this)
                .load(Uri.fromFile(new File(mVideoItem.getVideoPath())))
                .into(ivDetail);

        tvDetailPath.setText(mVideoItem.getVideoPath());
        tvDetailResolution.setText(mVideoItem.getVideoResolution());
        tvDetailSize.setText(String.format(
                getString(R.string.video_size), mVideoItem.getVideoSize()));
        tvDetailDuration.setText(mVideoItem.getVideoDuration());
        tvDetailDate.setText(mVideoItem.getVideoDate());
    }

    /**
     * @param context
     * @param videoItem
     */
    public static void activityStart(Context context, VideoItem videoItem) {
        Intent intent = new Intent(context, VideoDetailActivity.class);
        intent.putExtra("video_item", videoItem);
        context.startActivity(intent);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    @OnClick(R.id.float_btn_play)
    public void onViewClicked() {
        VideoPlayActivity.activityStart(this,mVideoItem);
    }
}
