package com.example.renkangchen.wifidirectdemo.bean;

import java.io.Serializable;

public class VideoItem implements Serializable {
    private String videoName;
    private String videoDuration;
    private String videoResolution;
    private String videoSize;
    private String videoDate;
    private String videoPath;
    private String videoCoverPath;

    public VideoItem() {
    }

    public VideoItem(String videoName, String videoDuration, String videoResolution, String videoSize, String videoDate, String videoPath, String videoCoverPath) {
        this.videoName = videoName;
        this.videoDuration = videoDuration;
        this.videoResolution = videoResolution;
        this.videoSize = videoSize;
        this.videoDate = videoDate;
        this.videoPath = videoPath;
        this.videoCoverPath = videoCoverPath;
    }

    public String getVideoName() {
        return videoName;
    }

    public void setVideoName(String videoName) {
        this.videoName = videoName;
    }

    public String getVideoDuration() {
        return videoDuration;
    }

    public void setVideoDuration(String videoDuration) {
        this.videoDuration = videoDuration;
    }

    public String getVideoResolution() {
        return videoResolution;
    }

    public void setVideoResolution(String videoResolution) {
        this.videoResolution = videoResolution;
    }

    public String getVideoSize() {
        return videoSize;
    }

    public void setVideoSize(String videoSize) {
        this.videoSize = videoSize;
    }

    public String getVideoDate() {
        return videoDate;
    }

    public void setVideoDate(String videoDate) {
        this.videoDate = videoDate;
    }

    public String getVideoPath() {
        return videoPath;
    }

    public void setVideoPath(String videoPath) {
        this.videoPath = videoPath;
    }

    public String getVideoCoverPath() {
        return videoCoverPath;
    }

    public void setVideoCoverPath(String videoCoverPath) {
        this.videoCoverPath = videoCoverPath;
    }
}

