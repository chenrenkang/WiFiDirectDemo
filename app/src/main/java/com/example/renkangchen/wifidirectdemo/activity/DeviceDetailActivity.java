package com.example.renkangchen.wifidirectdemo.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.example.renkangchen.wifidirectdemo.R;
import com.example.renkangchen.wifidirectdemo.bean.DeviceItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;

public class DeviceDetailActivity extends AppCompatActivity {
    private static final String TAG = "DeviceDetailActivity";

    @BindView(R.id.custom_toolbar)
    Toolbar mCustomToolbar;
    @BindView(R.id.tv_device_name)
    TextView mTvDeviceName;
    @BindView(R.id.tv_detail_cpu)
    TextView mTvDetailCpu;
    @BindView(R.id.tv_detail_ram)
    TextView mTvDetailRam;
    @BindView(R.id.tv_detail_mac)
    TextView mTvDetailMac;

    private DeviceItem mDeviceItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_detail);
        ButterKnife.bind(this);

        initData();
        initView();
    }

    public void initData() {
        mDeviceItem = (DeviceItem) getIntent().getSerializableExtra("device_item");
    }

    public void initView() {
        mCustomToolbar.setTitle(getResources().getString(R.string.device_detail));
        mCustomToolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));

        setSupportActionBar(mCustomToolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mTvDeviceName.setText(mDeviceItem.getDeviceName());
        mTvDetailCpu.setText(mDeviceItem.getDeviceCPU());
        mTvDetailRam.setText(mDeviceItem.getDeviceRAM());
        mTvDetailMac.setText(mDeviceItem.getDeviceMAC());
    }
    /**
     *
     * @param context
     * @param deviceItem
     */
    public static void activityStart(Context context, DeviceItem deviceItem) {
        Intent intent = new Intent(context,DeviceDetailActivity.class);
        intent.putExtra("device_item",deviceItem);
        context.startActivity(intent);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }
}
