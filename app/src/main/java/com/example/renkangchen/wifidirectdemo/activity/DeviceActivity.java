package com.example.renkangchen.wifidirectdemo.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.net.wifi.WpsInfo;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.renkangchen.wifidirectdemo.R;
import com.example.renkangchen.wifidirectdemo.adapter.DeviceListAdapter;
import com.example.renkangchen.wifidirectdemo.bean.DeviceItem;
import com.example.renkangchen.wifidirectdemo.bean.FileTransferBean;
import com.example.renkangchen.wifidirectdemo.bean.VideoItem;
import com.example.renkangchen.wifidirectdemo.broadcast.WiFiDirectBroadcastReceiver;
import com.example.renkangchen.wifidirectdemo.listener.FileTransferProgressListener;
import com.example.renkangchen.wifidirectdemo.listener.OnDeviceListItemListener;
import com.example.renkangchen.wifidirectdemo.listener.WiFiDirectActionListener;
import com.example.renkangchen.wifidirectdemo.service.WiFiP2PServerService;
import com.example.renkangchen.wifidirectdemo.util.CMDUtils;
import com.example.renkangchen.wifidirectdemo.util.DeviceInfoUtils;
import com.example.renkangchen.wifidirectdemo.util.FileUtils;
import com.example.renkangchen.wifidirectdemo.widget.MyLoadingDialog;
import com.example.renkangchen.wifidirectdemo.widget.MyProgressDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DeviceActivity extends AppCompatActivity implements
        OnDeviceListItemListener,
        WiFiDirectActionListener, FileTransferProgressListener {
    private static final String TAG = "DeviceActivity";

    private static final int PERMISSIONS_REQUEST_CODE_ACCESS_COARSE_LOCATION = 1001;

    @BindView(R.id.custom_toolbar)
    Toolbar mCustomToolbar;
    @BindView(R.id.iv_device_mine)
    ImageView mIvDeviceMine;
    @BindView(R.id.tv_device_mine_name)
    TextView mTvDeviceMineName;
    @BindView(R.id.recyclerview_available_dev)
    RecyclerView mRecyclerView;
    @BindView(R.id.fab_search)
    FloatingActionButton mFabShare;
    @BindView(R.id.tv_device_mine_status)
    TextView tvDeviceMineStatus;

    private boolean mWifiDirectEnabled = false;

    private VideoItem mVideoItem;

    private List<DeviceItem> mDeviceItemsList = new ArrayList<>();

    private DeviceListAdapter mDeviceListAdapter;

    private static final int CONNECT_DEVICE = 0;
    private static final int DISCONNECT_DEVICE = 1;
    private static final int SEND_FILE = 2;
    private static final int DEVICE_DETAIL = 3;

    private final IntentFilter mIntentFilter = new IntentFilter();
    private WifiP2pManager mWifiP2pManager;
    private WifiP2pManager.Channel mChannel;
    private WiFiDirectBroadcastReceiver mWiFiDirectBroadcastReceiver;

    private WifiP2pInfo mWifiP2pInfo;

    private MyLoadingDialog mSearchLoadingDialog;
    private MyProgressDialog mFileSendProgressDialog;
    private MyProgressDialog mFileReceiveProgressDialog;

    private static final int SHOW_RECEIVE_DIALOG = 1;
    private static final int UPDATE_RECEIVE_DIALOG = 2;
    private static final int CANCEL_RECEIVE_DIALOG = 3;
    private static final int SHOW_FILE_VERIFY_TOAST = 4;

    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case SHOW_RECEIVE_DIALOG:
                    String receiveFileMsg = String.format(
                            getResources().getString(R.string.receiving_file),
                            ((FileTransferBean) msg.obj).getFileName());
                    if (mFileReceiveProgressDialog == null) {
                        mFileReceiveProgressDialog = new MyProgressDialog(DeviceActivity.this);
                        mFileReceiveProgressDialog.updateProgress(0);
                        mFileReceiveProgressDialog.show(receiveFileMsg,
                                false,
                                false);
                    }
                    break;
                case UPDATE_RECEIVE_DIALOG:
                    mFileReceiveProgressDialog.updateProgress(msg.arg1);
                    break;
                case CANCEL_RECEIVE_DIALOG:
                    Log.d(TAG, "handleMessage: dialog cancel");
                    mFileReceiveProgressDialog.cancel();
                    break;
                case SHOW_FILE_VERIFY_TOAST:
                    String verifyFileMsg = msg.arg1 == 0
                            ? getString(R.string.file_verify_correct)
                            : getString(R.string.file_verify_error);

                    Toast.makeText(DeviceActivity.this,verifyFileMsg,Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device);
        ButterKnife.bind(this);

        getLocationPermission();

        initData();
        initView();
    }

    private void initData() {
        mVideoItem = (VideoItem) getIntent().getSerializableExtra("video_item");

        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_DISCOVERY_CHANGED_ACTION);

        mWifiP2pManager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
        mChannel = mWifiP2pManager.initialize(this, getMainLooper(), null);

        startWifiP2PServerService();
    }

    private void initView() {
        mCustomToolbar.setTitle(getResources().getString(R.string.device_list_select));
        mCustomToolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));

        setSupportActionBar(mCustomToolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mTvDeviceMineName.setText(DeviceInfoUtils.getDeviceName(this));
        setMyDeviceLogo();

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mDeviceListAdapter = new DeviceListAdapter(this, mDeviceItemsList, this);
        mRecyclerView.setAdapter(mDeviceListAdapter);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    private void setMyDeviceLogo() {
        String brand = DeviceInfoUtils.getDeviceBrand().toLowerCase();

        if ("honor".equals(brand)) {
            Glide.with(this)
                    .load(R.drawable.honor_logo)
                    .into(mIvDeviceMine);
        } else if ("huawei".equals(brand)) {
            mIvDeviceMine.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            Glide.with(this)
                    .load(R.drawable.huawei_logo)
                    .into(mIvDeviceMine);
        }
    }

    public static void activityStart(Context context, VideoItem videoItem) {
        Intent intent = new Intent(context, DeviceActivity.class);
        intent.putExtra("video_item", videoItem);
        context.startActivity(intent);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onDeviceItemClick(DeviceItem deviceItem) {

    }

    @Override
    public void onDeviceItemMoreClick(DeviceItem deviceItem) {
        showVideoItemMoreDialog(deviceItem);
    }

    @Override
    protected void onResume() {
        super.onResume();

        mWiFiDirectBroadcastReceiver = new WiFiDirectBroadcastReceiver(mWifiP2pManager,
                mChannel, this);
        registerReceiver(mWiFiDirectBroadcastReceiver, mIntentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mWiFiDirectBroadcastReceiver);
        disconnectDevice();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        disconnectDevice();
        stopService(new Intent(this, WiFiP2PServerService.class));
    }

    @Override
    public void onWiFiP2PEnabled(boolean enabled) {
        Log.d(TAG, "onWiFiP2PEnabled: " + enabled);
        mWifiDirectEnabled = enabled;
        if (!enabled) {
            Toast.makeText(this,
                    getString(R.string.wifip2p_disable),
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onSelfDeviceAvailable(WifiP2pDevice wifiP2pDevice) {
        mTvDeviceMineName.setText(wifiP2pDevice.deviceName);
        tvDeviceMineStatus.setText(getDeviceStatus(wifiP2pDevice.status));
    }

    @Override
    public void onPeersAvailable(Collection<WifiP2pDevice> wifiP2pDeviceList, WifiP2pDeviceList peers) {
        Log.d(TAG, "onPeersAvailable: size is " + wifiP2pDeviceList.size());

        if (wifiP2pDeviceList.size() > 0) {
            mDeviceItemsList.clear();

            for (Object object : wifiP2pDeviceList) {
                WifiP2pDevice wifiP2pDevice = (WifiP2pDevice) object;

                String deviceName = wifiP2pDevice.deviceName;
                String deviceMAC = wifiP2pDevice.deviceAddress;

                DeviceItem deviceItem = new DeviceItem(deviceName, deviceMAC, wifiP2pDevice);
                deviceItem.setDeviceStatus(getDeviceStatus(wifiP2pDevice.status));

                addDeviceItemIntoList(deviceItem);

                Log.d(TAG, "onPeersAvailable: " + wifiP2pDevice.deviceName);
            }
        }
    }

    @Override
    public void onWiFiDirectDiscoveryState(int state) {

        if (state == WifiP2pManager.WIFI_P2P_DISCOVERY_STARTED) {
            Log.d(TAG, "onWiFiDirectDiscoveryState: start");
        } else if (state == WifiP2pManager.WIFI_P2P_DISCOVERY_STOPPED) {
            Log.d(TAG, "onWiFiDirectDiscoveryState: stop");
            if (mSearchLoadingDialog != null) {
                mSearchLoadingDialog.cancel();
            }
        }
    }

    private void connectDevice(WifiP2pDevice wifiP2pDevice) {
        Log.d(TAG, "connectDevice: " + wifiP2pDevice.deviceName);

        WifiP2pConfig config = new WifiP2pConfig();
        config.deviceAddress = wifiP2pDevice.deviceAddress;
        config.wps.setup = WpsInfo.PBC;

        mWifiP2pManager.connect(mChannel, config, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                Log.d(TAG, "onSuccess: connect device");
            }

            @Override
            public void onFailure(int reason) {
                String msg = String.format(
                        getResources().getString(R.string.connection_fail),
                        String.valueOf(reason));
                Toast.makeText(DeviceActivity.this, msg,
                        Toast.LENGTH_SHORT).show();

                Log.d(TAG, "onFailure: connect device reason is " + reason);
            }
        });
    }

    private void disconnectDevice() {
        mWifiP2pManager.removeGroup(mChannel, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                Log.d(TAG, "onSuccess: disconnect device");
            }

            @Override
            public void onFailure(int reason) {
                Log.d(TAG, "onFailure: disconnect device fail for reason " + reason);
            }
        });
    }

    @Override
    public void onConnectionInfoAvailable(WifiP2pInfo wifiP2pInfo) {
        if (mSearchLoadingDialog != null) {
            mSearchLoadingDialog.cancel();
        }

        Log.d(TAG, "onConnectionInfoAvailable: groupedFormed "
                + wifiP2pInfo.groupFormed);
        Log.d(TAG, "onConnectionInfoAvailable: isGroupOwner "
                + wifiP2pInfo.isGroupOwner);
        Log.d(TAG, "onConnectionInfoAvailable: getHostAddress "
                + wifiP2pInfo.groupOwnerAddress.getHostAddress());

        mWifiP2pInfo = wifiP2pInfo;

        if (wifiP2pInfo.groupFormed && !wifiP2pInfo.isGroupOwner) {
            tvDeviceMineStatus.setText(getString(R.string.is_client));
        } else {
            tvDeviceMineStatus.setText(getString(R.string.is_server));
            startService(new Intent(this, WiFiP2PServerService.class));
        }
    }

    @Override
    public void onFileTransferProgressChanged(FileTransferBean fileTransferBean, int progress) {
        Log.d(TAG, "onFileTransferProgressChanged: " + progress);

        if (mFileReceiveProgressDialog == null) {
            Log.d(TAG, "onFileTransferProgressChanged: dialog is null");
            Message message = new Message();
            message.what = SHOW_RECEIVE_DIALOG;
            message.obj = fileTransferBean;

            mHandler.sendMessage(message);
        } else {
            Message message = new Message();
            message.what = UPDATE_RECEIVE_DIALOG;
            message.arg1 = progress;

            if (progress == 100) {
                mHandler.sendEmptyMessage(CANCEL_RECEIVE_DIALOG);
            } else {
                mHandler.sendMessage(message);
            }
        }

    }

    @Override
    public void onFilTransferDone(File file,String client_ip) {
        mHandler.sendEmptyMessage(CANCEL_RECEIVE_DIALOG);
        Log.d(TAG, "onFilTransferDone: "+client_ip);
    }

    @Override
    public void onFileVerifyResult(boolean fileVerifyResult) {
        Log.d(TAG, "onFileVerifyResult: "+fileVerifyResult);

        Message message = new Message();
        message.what = SHOW_FILE_VERIFY_TOAST;

        if (fileVerifyResult){
            message.arg1 = 0;
        } else {
            message.arg1 = -1;
        }

        mHandler.sendMessage(message);
    }

    @Override
    public void onDisconnection() {
        Log.d(TAG, "onDisconnection: ");
        mWifiP2pInfo = null;
    }

    @Override
    public void onChannelDisconnected() {
        Log.d(TAG, "onChannelDisconnected: ");
    }

    public void showVideoItemMoreDialog(DeviceItem deviceItem) {
        String[] items = getResources().getStringArray(R.array.device_item_more);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case CONNECT_DEVICE:
                        connectDevice(deviceItem.getWifiP2pDevice());
                        break;
                    case DISCONNECT_DEVICE:
                        disconnectDevice();
                        break;
                    case SEND_FILE:
                        String msg = String.format(
                                getResources().getString(R.string.send_file_msg),
                                mVideoItem.getVideoName(),
                                deviceItem.getDeviceName());

                        mFileSendProgressDialog = new MyProgressDialog(DeviceActivity.this);
                        mFileSendProgressDialog.show(msg, false, false);
                        mFileSendProgressDialog.updateProgress(0);

                        sendFile();
                        break;
                    case DEVICE_DETAIL:
                        DeviceDetailActivity.activityStart(DeviceActivity.this, deviceItem);
                        break;
                }
            }
        });

        builder.create().show();
    }

    private void sendFile() {
        File file = new File(mVideoItem.getVideoPath());
        if (file.exists()) {
            FileTransferBean fileTransferBean = new FileTransferBean(file.getPath(),
                    file.length());
            Log.d(TAG, "sendFile: File to Send: " + fileTransferBean);
            new FileSendTask(DeviceActivity.this, fileTransferBean)
                    .execute(mWifiP2pInfo.groupOwnerAddress.getHostAddress());
        }
    }

    @Override
    public void onBackPressed() {
        AlertDialog dialog = new AlertDialog.Builder(DeviceActivity.this).create();
        dialog.setTitle(getResources().getString(R.string.send_file_now));
        dialog.setMessage(getResources().getString(R.string.confirm_cancel_send));

        dialog.setButton(DialogInterface.BUTTON_POSITIVE,
                getResources().getString(R.string.confirm),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        DeviceActivity.super.onBackPressed();
                    }
                });
        dialog.setButton(DialogInterface.BUTTON_NEGATIVE,
                getResources().getString(R.string.cancel),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();

                    }
                });
        dialog.show();
    }

    @OnClick(R.id.fab_search)
    public void onViewClicked() {
        if (mWifiDirectEnabled) {
            mSearchLoadingDialog = new MyLoadingDialog(this);

            mSearchLoadingDialog.show(getResources().getString(R.string.search_device_loading),
                    true,
                    true);

            mWifiP2pManager.discoverPeers(mChannel, new WifiP2pManager.ActionListener() {
                @Override
                public void onSuccess() {
                    Log.d(TAG, "onSuccess: discoverPeers");
                }

                @Override
                public void onFailure(int reason) {
                    Log.d(TAG, "onFailure: discoverPeers reason for failure " + reason);
                }
            });
        }
    }

    private void getLocationPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    DeviceActivity.PERMISSIONS_REQUEST_CODE_ACCESS_COARSE_LOCATION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_CODE_ACCESS_COARSE_LOCATION) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.d(TAG, "onRequestPermissionsResult: ");
            }
        }
    }

    private void addDeviceItemIntoList(DeviceItem deviceItem) {
        boolean sameDevice = false;
        for (DeviceItem item : mDeviceItemsList) {
            if (deviceItem.getDeviceMAC().equals(item.getDeviceMAC())) {
                sameDevice = true;
                break;
            }
        }

        if (!sameDevice) {
            mDeviceItemsList.add(deviceItem);
            mRecyclerView.getAdapter().notifyDataSetChanged();
        }
    }

    public class FileSendTask extends AsyncTask<String, Integer, Boolean> {
        private FileTransferBean fileTransferBean;
        private static final int SOCKET_PORT = 8888;
        private static final int TIMEOUT = 10000;

        private static final String TAG = "FileSendTask";

        private Socket socket = null;
        private OutputStream outputStream = null;
        private ObjectOutputStream objectOutputStream = null;
        private InputStream inputStream = null;

        private int priorProgress = 0;


        public FileSendTask(Context context, FileTransferBean fileTransferBean) {
            this.fileTransferBean = fileTransferBean;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (mFileSendProgressDialog != null) {
                mFileSendProgressDialog.show(fileTransferBean.getFileName(), false, false);
                mFileSendProgressDialog.updateProgress(0);
            } else {
                Log.d(TAG, "onPreExecute: file send progress dialog is null");
            }
        }

        @Override
        protected Boolean doInBackground(String... strings) {
            fileTransferBean.setFileMD5(
                    FileUtils.getFileMD5(new File(fileTransferBean.getFilePath())));

            Log.d(TAG, "doInBackground: " + fileTransferBean.getFileName()
                    + " MD5 is " + fileTransferBean.getFileMD5());

            try {
                socket = new Socket();
                socket.bind(null);
                socket.connect((new InetSocketAddress(strings[0], SOCKET_PORT)),
                        TIMEOUT);

                outputStream = socket.getOutputStream();
                objectOutputStream = new ObjectOutputStream(outputStream);

                objectOutputStream.writeObject(fileTransferBean);

                inputStream = new FileInputStream(new File(fileTransferBean.getFilePath()));

                long fileLength = fileTransferBean.getFileLength();
                long total = 0;
                byte[] buf = new byte[512];
                int len;

                while ((len = inputStream.read(buf)) != -1) {
                    outputStream.write(buf, 0, len);
                    total += len;
                    int progress = (int) ((total * 100) / fileLength);

                    if (priorProgress != progress) {
                        publishProgress(progress);
                        priorProgress = progress;
                    }

                    //Log.d(TAG, "doInBackground: File Send Progress--->" + progress);
                }

                outputStream.close();
                objectOutputStream.close();
                inputStream.close();
                socket.close();

                outputStream = null;
                objectOutputStream = null;
                inputStream = null;
                socket = null;

                Log.d(TAG, "doInBackground: File Send Success");

                return true;
            } catch (Exception e) {
                Log.e(TAG, "doInBackground: File Send Error " + e.getMessage());
            } finally {
                cleanUp();
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            if (mFileSendProgressDialog != null){
                if (values[0] % 5 == 0) {
                    Log.d(TAG, "onProgressUpdate: progress--->" + values[0]);
                    mFileSendProgressDialog.updateProgress(values[0]);
                }
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            Log.d(TAG, "onPostExecute: " + aBoolean);
            if (mFileSendProgressDialog != null) {
                mFileSendProgressDialog.cancel();
            }
        }

        private void cleanUp() {
            if (outputStream != null) {
                try {
                    outputStream.close();
                    outputStream = null;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (inputStream != null) {
                try {
                    inputStream.close();
                    inputStream = null;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (objectOutputStream != null) {
                try {
                    objectOutputStream.close();
                    objectOutputStream = null;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (socket != null) {
                try {
                    socket.close();
                    socket = null;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    private void startWifiP2PServerService() {
        WiFiP2PServerService.setFileTransferProgressListener(this);
        Intent intent = new Intent(DeviceActivity.this,WiFiP2PServerService.class);
        this.startService(intent);
    }

    private String getDeviceStatus(int deviceStatus) {
        Log.d(TAG, "Peer status :" + deviceStatus);
        switch (deviceStatus) {
            case WifiP2pDevice.AVAILABLE:
                return getString(R.string.device_status_available);
            case WifiP2pDevice.INVITED:
                return getString(R.string.device_status_invited);
            case WifiP2pDevice.CONNECTED:
                return getString(R.string.device_status_connected);
            case WifiP2pDevice.FAILED:
                return getString(R.string.device_status_failed);
            case WifiP2pDevice.UNAVAILABLE:
                return getString(R.string.device_status_unavailable);
            default:
                return getString(R.string.device_status_unknown);
        }
    }

}
