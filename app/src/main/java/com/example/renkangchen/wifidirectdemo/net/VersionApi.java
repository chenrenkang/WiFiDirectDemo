package com.example.renkangchen.wifidirectdemo.net;

import com.example.renkangchen.wifidirectdemo.bean.VersionBean;

import retrofit2.Call;
import retrofit2.http.GET;

public interface VersionApi {

    @GET("/version/getLatestVersion")
    Call<VersionBean> getLatestVersion();
}
