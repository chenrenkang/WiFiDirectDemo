package com.example.renkangchen.wifidirectdemo.widget;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.renkangchen.wifidirectdemo.R;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MyLoadingDialog extends Dialog {
    private static final String TAG = "MyLoadingDialog";
    @BindView(R.id.tv_loading_message)
    TextView tvLoadingMessage;

    public MyLoadingDialog(@NonNull Context context) {
        super(context);

        setContentView(R.layout.my_loading_dialog_layout);
        ButterKnife.bind(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ViewGroup.LayoutParams params = getWindow().getAttributes();
        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);
    }

    public void show(String msg,boolean cancelable,boolean canceledOnTouchOutside) { 
        setCancelable(cancelable);
        setCanceledOnTouchOutside(canceledOnTouchOutside);
        if (tvLoadingMessage != null) {
            tvLoadingMessage.setText(msg);
        }
        show();
    }

    public void show(@StringRes  int msgRes, boolean cancelable, boolean canceledOnTouchOutside) {
        setCancelable(cancelable);
        setCanceledOnTouchOutside(canceledOnTouchOutside);

        tvLoadingMessage.setText(msgRes);
        show();
    }
}
