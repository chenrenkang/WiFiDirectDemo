package com.example.renkangchen.wifidirectdemo.util;

import android.app.ActivityManager;
import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;

public class DeviceInfoUtils {
    private static final String TAG = "DeviceInfoUtils";

    /**
     * get device manufacturer ,
     * take honor 8 as example,the result is
     * HUAWEI
     * @return
     */
    public static String getDeviceManufacturer() {
        return Build.MANUFACTURER;
    }

    /**
     * get device product name
     * FRD-AL10
     * @return
     */
    public static String getDeviceProduct() {
        return Build.PRODUCT;
    }

    /**
     * get device brand name
     * honor
     * @return
     */
    public static String getDeviceBrand() {
        return Build.BRAND;
    }

    /**
     * get device model (the same as get product)
     * @return
     */
    public static String getDeviceModel() {
        return Build.MODEL;
    }

    /**
     * get device RAM size
     * @param context
     * @return
     */
    public static double getDeviceRAM(Context context) {
        ActivityManager activityManager = (ActivityManager) context
                .getSystemService(Context.ACTIVITY_SERVICE);
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();

        activityManager.getMemoryInfo(memoryInfo);
        return (double)memoryInfo.totalMem/(1024*1024);
    }
    /**
     *
     * @param context
     * @return
     */
    public static  String getDeviceMAC(Context context) {
      return "";
    }

    /**
     * get the device name
     * Honor 8
     * this value could change after modifying it
     *
     * @param context
     * @return
     */
    public static String getDeviceName(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), "bluetooth_name");
    }

}
