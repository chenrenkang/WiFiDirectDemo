package com.example.renkangchen.wifidirectdemo.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.example.renkangchen.wifidirectdemo.R;
import com.example.renkangchen.wifidirectdemo.util.LanguageUtil;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SettingActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    private static final String TAG = "SettingActivity";

    @BindView(R.id.custom_toolbar)
    Toolbar mCustomToolbar;
    @BindView(R.id.spinner_language)
    Spinner mSpinnerLanguage;

    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mEditor;
    private int mCurrentLanguageType;
    private int mLanguageType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        ButterKnife.bind(this);

        initData();
        initView();
    }

    private void initData() {
        mSharedPreferences = getSharedPreferences("userpreference", Context.MODE_PRIVATE);
        mEditor = mSharedPreferences.edit();
        mCurrentLanguageType = mSharedPreferences.getInt("language_type", 0);
    }

    private void initView() {
        mCustomToolbar.setTitle(getResources().getString(R.string.my_preference));
        mCustomToolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));

        setSupportActionBar(mCustomToolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mSpinnerLanguage.setOnItemSelectedListener(this);
        Log.d(TAG, "initView: "+mCurrentLanguageType);
        mSpinnerLanguage.setSelection(mCurrentLanguageType, true);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        mLanguageType = position;

        if (mCurrentLanguageType != mLanguageType) {
            LanguageUtil.changeLanguage(this,mLanguageType);
            mEditor.putInt("language_type", mLanguageType);

            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    /**
     * @param context
     */
    public static void activityStart(Context context) {
        Intent intent = new Intent(context, SettingActivity.class);
        context.startActivity(intent);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mEditor != null) {
            mEditor.commit();
        }
    }
}
