package com.example.renkangchen.wifidirectdemo.util;

import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.MessageDigest;

public class FileUtils {
    private static final String TAG = "FileUtils";
    /**
     * get file dir from its path
     *
     * @param path
     * @return
     */
    public static String getDirFromPath(String path) {
        return path.substring(0,path.lastIndexOf("/"));
    }

    /**
     *
     * @param path
     * @return
     */
    public static String getFileSuffix(String path) {
        return  path.substring(path.lastIndexOf("."),path.length());
    }

    /**
     *
     * @param filePath
     * @return
     */
    public static long getFileSize(String filePath) {
        File file = new File(filePath);
        if (file.exists() && file.isFile()) {
            return file.length();
        } else {
            Log.e(TAG, "getFileSize: file not exists or not a file");
            return 0;
        }
    }

    /**
     *
     * @param file
     * @return
     */
    public static String getFileMD5(File file) {
        InputStream inputStream = null;
        byte [] buffer = new byte[2048];
        int numRead;
        MessageDigest md5;
        try {
            inputStream = new FileInputStream(file);
            md5 = MessageDigest.getInstance("MD5");
            while ((numRead = inputStream.read(buffer)) > 0) {
                md5.update(buffer,0,numRead);
            }
            inputStream.close();
            inputStream = null;

            return md52String(md5.digest());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if(inputStream != null) {
                try {
                    inputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     *
     * @param md5Bytes
     * @return
     */
    private static String md52String(byte[] md5Bytes) {
       StringBuilder hexValue = new StringBuilder();
       for (byte b :md5Bytes) {
           int val = ((int) b) & 0xff;
           if (val < 16) {
               hexValue.append("0");
           }
           hexValue.append(Integer.toHexString(val));
       }
       return hexValue.toString();
    }
}
