package com.example.renkangchen.wifidirectdemo.bean;

import android.net.wifi.p2p.WifiP2pDevice;

import java.io.Serializable;

public class DeviceItem implements Serializable {
    private String deviceName;
    private String deviceModel;
    private String deviceCPU;
    private String deviceRAM;
    private String deviceMAC;
    private String deviceBattery;
    private String deviceStatus;
    private WifiP2pDevice wifiP2pDevice;

    public DeviceItem() {
    }

    public DeviceItem(String deviceName, String deviceMAC, WifiP2pDevice wifiP2pDevice) {
        this.deviceName = deviceName;
        this.deviceMAC = deviceMAC;
        this.wifiP2pDevice = wifiP2pDevice;
    }

    public DeviceItem(String deviceName, String deviceModel, String deviceCPU, String deviceRAM, String deviceMAC, String deviceBattery, String deviceStatus, WifiP2pDevice wifiP2pDevice) {
        this.deviceName = deviceName;
        this.deviceModel = deviceModel;
        this.deviceCPU = deviceCPU;
        this.deviceRAM = deviceRAM;
        this.deviceMAC = deviceMAC;
        this.deviceBattery = deviceBattery;
        this.deviceStatus = deviceStatus;
        this.wifiP2pDevice = wifiP2pDevice;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getDeviceModel() {
        return deviceModel;
    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    public String getDeviceCPU() {
        return deviceCPU;
    }

    public void setDeviceCPU(String deviceCPU) {
        this.deviceCPU = deviceCPU;
    }

    public String getDeviceRAM() {
        return deviceRAM;
    }

    public void setDeviceRAM(String deviceRAM) {
        this.deviceRAM = deviceRAM;
    }

    public String getDeviceMAC() {
        return deviceMAC;
    }

    public void setDeviceMAC(String deviceMAC) {
        this.deviceMAC = deviceMAC;
    }

    public String getDeviceBattery() {
        return deviceBattery;
    }

    public void setDeviceBattery(String deviceBattery) {
        this.deviceBattery = deviceBattery;
    }

    public String getDeviceStatus() {
        return deviceStatus;
    }

    public void setDeviceStatus(String deviceStatus) {
        this.deviceStatus = deviceStatus;
    }

    public WifiP2pDevice getWifiP2pDevice() {
        return wifiP2pDevice;
    }

    public void setWifiP2pDevice(WifiP2pDevice wifiP2pDevice) {
        this.wifiP2pDevice = wifiP2pDevice;
    }

    public static DeviceItem getOneDummyDeviceItem() {
        DeviceItem dummy_item = new DeviceItem("HUAWEI P30",
                "VOG-AL00",
                "HUAWEI Kirin 980",
                "16G",
                "30:3a:64:d0:73:75",
                "80%",
                "Unknown",
                null);

        return dummy_item;
    }
}
