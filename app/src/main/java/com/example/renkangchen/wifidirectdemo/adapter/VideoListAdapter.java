package com.example.renkangchen.wifidirectdemo.adapter;

import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.bumptech.glide.annotation.GlideModule;
import com.example.renkangchen.wifidirectdemo.bean.VideoItem;
import com.example.renkangchen.wifidirectdemo.R;
import com.example.renkangchen.wifidirectdemo.fragment.VideoFragment;

import java.io.File;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class VideoListAdapter extends RecyclerView.Adapter<VideoListAdapter.ViewHolder> {
    private static final String TAG = "VideoListAdapter";
    private final List<VideoItem> mValues;
    private final VideoFragment.OnVideoFragmentInteractionListener mListener;


    public VideoListAdapter(List<VideoItem> mValues, VideoFragment.OnVideoFragmentInteractionListener mListener) {
        this.mValues = mValues;
        this.mListener = mListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.video_item_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);

        Glide.with(holder.mView.getContext())
                .load(Uri.fromFile(new File(holder.mItem.getVideoPath())))
                .into(holder.ivVideoThumbnail);

        holder.tvVideoTitle.setText(holder.mItem.getVideoName());
        holder.tvVideoDuration.setText(holder.mItem.getVideoDuration());
        holder.tvVideoResolution.setText(holder.mItem.getVideoResolution());

        holder.cardViewVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onVideoFragmentItemClick(holder.mItem);
            }
        });

        holder.imgBtnVideoMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onVideoFragmentItemMore(holder.mItem,position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    /* ensure item has unique id */
    @Override
    public long getItemId(int position) {
        return mValues.get(position).hashCode();
    }

    /**
     *
     * @param position
     */
    public void removeData(int position) {
       mValues.remove(position);
       notifyItemRemoved(position);
       notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public VideoItem mItem;

        @BindView(R.id.iv_video_thumbnail)
        ImageView ivVideoThumbnail;
        @BindView(R.id.tv_video_title)
        TextView tvVideoTitle;
        @BindView(R.id.img_btn_video_more)
        ImageButton imgBtnVideoMore;
        @BindView(R.id.tv_video_duration)
        TextView tvVideoDuration;
        @BindView(R.id.tv_video_resolution)
        TextView tvVideoResolution;
        @BindView(R.id.cardview_video)
        CardView cardViewVideo;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            ButterKnife.bind(this, mView);
        }
    }
}
